package ua.kharkov.khpi.korshun.lab02;

import java.io.Serializable;

/**
 * Зберігає вихідні дані та результат обчислення.
 * 
 * @author Коршун Олексій
 * @version 1.0
 */
public class Item implements Serializable {
	/** Автоматично сгенерована константа */
	private static final long serialVersionUID = 1L;
	/** Аргумент обчисляємой функціі */
	transient private int x;

	/** Результат обчислення */
	private int y;

	/** Ініціалізує поля {@linkplain Item#x}, {@linkplain Item#y} */
	public Item() {
		x = 0;
		y = 0;
	}

	/**
	 * Получення значенення поля {@linkplain Item#x}
	 * 
	 * @return Значення {@linkplain Item#x}
	 */
	public double getX() {
		return x;
	}

	/**
	 * Встановка значення поля {@linkplain Item # x}
	 * 
	 * @param x - значення для {@linkplain Item # x}
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Получення значенення поля {@linkplain Item#y}
	 * 
	 * @return Значення {@linkplain Item#y}
	 */
	public int getY() {
		return y;
	}

	/**
	 * Встановка значення поля {@linkplain Item # y}
	 * 
	 * @param y - значення для {@linkplain Item # y}
	 */
	public void setY(int y) {
		this.y = y;
	}

}
