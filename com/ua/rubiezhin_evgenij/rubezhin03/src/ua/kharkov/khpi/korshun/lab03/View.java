package ua.kharkov.khpi.korshun.lab03;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Product (Шаблон проектування Factory Method) фнтерфейс "фабрікуемих" об'єктів
 * Оголошує методи іідображення об'єктів
 *
 * @Author Коршун Олексій
 * @Version 1.0  
 */
public interface View {

	/** Показує заголовок */
	public void viewHeader();

	/** Показує основну частину */
	public void viewBody();

	/** Показує закінчення */
	public void viewFooter();

	/** Показує об'єкт цілком */
	public void viewShow();

	/** Виконує ініціалізацію */
	public void viewInit(int x);

	/** Зберігає дані для подальшого відновлення */
	public void viewSave() throws IOException;

	/** Відновлює раніше збережені дані */
	public void viewRestore() throws FileNotFoundException, IOException, ClassNotFoundException;
}
