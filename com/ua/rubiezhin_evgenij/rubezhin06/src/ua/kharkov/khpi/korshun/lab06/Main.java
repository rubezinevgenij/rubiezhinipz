package ua.kharkov.khpi.korshun.lab06;

import ua.kharkov.khpi.korshun.lab03.View;
import ua.kharkov.khpi.korshun.lab03.ViewableResult;
import ua.kharkov.khpi.korshun.lab05.DeleteItemConsoleCommand;
import ua.kharkov.khpi.korshun.lab05.FindConsoleCommand;
import ua.kharkov.khpi.korshun.lab05.GenerateConsoleCommand;
import ua.kharkov.khpi.korshun.lab05.Menu;
import ua.kharkov.khpi.korshun.lab05.ViewConsoleCommand;

/**
 * Обчислення і відображення Результатів; містить реалізацію Статичного методу
 * main ()
 * 
 * @Author Коршун Олексій
 * @Version 5.0
 * @See Main # main
 */
public class Main {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; Обслуговує колекцію
	 * об'єктів {@linkplain ua.kharkov.khpi.korshun.lab03.Item}; ініціалізується
	 * за допомогою Factory Method
	 */
	private View view = new ViewableResult().getView();

	/**
	 * Об'єкт класу {@linkplain Menu}; Макрокоманда (шаблон Command)
	 */
	private Menu menu = new Menu();

	/** Обробка команд користувача */
	public void run() {
		menu.add(new ViewConsoleCommand(view));
		menu.add(new GenerateConsoleCommand(view));
		menu.add(new FindConsoleCommand(view));
		menu.add(new DeleteItemConsoleCommand(view));
		menu.add(new ExecuteConsoleCommand(view));
		menu.execute();
	}

	/**
	 * Виконується при запуску програми
	 * 
	 * @Param args параметри запуску програми
	 */
	public static void main(String[] args) {

		Main menu = new Main();
		menu.run();

	}

}
