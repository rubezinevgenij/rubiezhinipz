package ua.kharkov.khpi.korshun.lab05;

import ua.kharkov.khpi.korshun.lab03.View;

/**
 * Консольна команда View; шаблон Command
 * 
 * @author Коршун Олексій
 * @version 1.0
 */
public class ViewConsoleCommand implements ConsoleCommand {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; Обслуговує колекцію
	 * об'єктів {@linkplain ua.kharkov.khpi.korshun.lab03.Item}  
	 */
	private View view;

	/**
	 * Ініціалізує поле {@linkplain ViewConsoleCommand # view}
	 * 
	 * @Param view об'єкт, який реалізує інтерфейс {@linkplain View}  
	 */
	public ViewConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'v';
	}

	@Override
	public String toString() {
		return "'v'iew";
	}

	@Override
	public void execute() {
		System.out.println("View current.");
		view.viewShow();
	}
}
