package ua.kharkov.khpi.korshun.lab05;

/**
 * Інтерфейс Консольної команди; Шаблон Command
 * 
 * @Author Коршун Олексій
 * @Version 1.0  
 */
public interface ConsoleCommand extends Command {

	/**
	 * Гаряча клавіша команди; Шаблон Command
	 * 
	 * @Return символ гарячої клавіші  
	 */
	public char getKey();
}
