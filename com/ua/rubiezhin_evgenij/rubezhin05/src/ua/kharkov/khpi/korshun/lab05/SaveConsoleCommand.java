package ua.kharkov.khpi.korshun.lab05;

import ua.kharkov.khpi.korshun.lab03.View;

/**
 * Консольна команда Save; шаблон Command
 * 
 * @author Коршун Олексій
 * @version 1.0
 */
public class SaveConsoleCommand implements ConsoleCommand {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; Обслуговує колекцію
	 * об'єктів {@linkplain ua.kharkov.khpi.korshun.lab03.Item}  
	 */
	private View view;

	/**
	 * Ініціалізує поле {@linkplain SaveConsoleCommand # view}
	 * 
	 * @Param view об'єкт, який реалізує інтерфейс {@linkplain View}  
	 */
	public SaveConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 's';
	}

	@Override
	public String toString() {
		return "'s'ave";
	}

	@Override
	public void execute() {
		System.out.println("Save current.");

		try {
			view.viewSave();
		} catch (Exception e) {
			System.err.println("Serealization error: " + e);
		}
		view.viewShow();
	}
}
