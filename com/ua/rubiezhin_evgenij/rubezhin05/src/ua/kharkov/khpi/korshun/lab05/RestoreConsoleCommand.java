package ua.kharkov.khpi.korshun.lab05;

import ua.kharkov.khpi.korshun.lab03.View;

/**
 * Консольна команда Restore; шаблон Command
 * 
 * @author Коршун Олексій
 * @version 1.0
 */
public class RestoreConsoleCommand implements ConsoleCommand {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; Обслуговує колекцію
	 * об'єктів {@linkplain ua.kharkov.khpi.korshun.lab03.Item}  
	 */
	private View view;

	/**
	 * Ініціалізує поле {@linkplain RestoreConsoleCommand # view}
	 * 
	 * @Param view об'єкт, який реалізує інтерфейс {@linkplain View}  
	 */
	public RestoreConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'r';
	}

	@Override
	public String toString() {
		return "'r'estore";
	}

	@Override
	public void execute() {
		System.out.println("Restore last saved.");

		try {
			view.viewRestore();
		} catch (Exception e) {
			System.err.println("Serealization error: " + e);
		}
		view.viewShow();
	}
}
