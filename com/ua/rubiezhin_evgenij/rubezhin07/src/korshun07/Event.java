package korshun07;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Анотація часу виконання для призначення методам спостерігача конкретних подій
 * 
 * @Author Коршун Олексій
 * @See AnnotatedObserver  
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
	String value();
}
