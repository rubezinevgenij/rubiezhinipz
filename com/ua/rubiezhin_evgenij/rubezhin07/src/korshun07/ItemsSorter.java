package korshun07;

import java.util.Collections;

/**
 * Спостерігач; визначає методи обробки подій; використовує Event; шаблон
 * Observer
 * 
 * @author Коршун Олексій
 * @see AnnotatedObserver
 * @see Event
 */
public class ItemsSorter extends AnnotatedObserver {
	/** Константа-ідентифікатор події, оброблюваного спостерігачами */
	public static final String ITEMS_SORTED = "ITEMS_SORTED";

	/**
	 * Оброблювач події {@linkplain Items # ITEMS_CHANGED}; сповіщає
	 * Спостерігачів; шаблон Observer
	 *
	 * @Param observable Спостережуваний об'єкт класу {@linkplain Items}
	 * @See Observable
	 */
	@Event(Items.ITEMS_CHANGED)
	public void itemsChanged(Items observable) {
		Collections.sort(observable.getItems());
		observable.call(ITEMS_SORTED);
	}

	/**
	 * Оброблювач події {@linkplain Items # ITEMS_SORTED}; шаблон Observer
	 *
	 * @Param observable Спостережуваний об'єкт класу {@linkplain Items}
	 * @See Observable
	 */
	@Event(ITEMS_SORTED)
	public void itemsSorted(Items observable) {
		System.out.println(observable.getItems());
	}

	/**
	 * Оброблювач події {@linkplain Items # ITEMS_Removed}; шаблон Observer
	 *
	 * @Param observable Спостережуваний об'єкт класу {@linkplain Items}
	 * @See Observable
	 */
	@Event(Items.ITEMS_REMOVED)
	public void itemsRemoved(Items observable) {
		System.out.println(observable.getItems());
	}
}
