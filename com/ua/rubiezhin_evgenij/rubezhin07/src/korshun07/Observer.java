package korshun07;

/**
 * Являє метод Для взаємодії Спостережуваного об'єкта спостерігача; Шаблон
 * Observer
 * 
 * @Author Коршун Олексій
 * @Version 1.0
 * 
 * @See Observable  
 */
public interface Observer {
	/**
	 * Викликається спостережуваним об'єктом для кожного спостерігача; шаблон
	 * Observer
	 *
	 * @Param observable Посилання на спостережуваний об'єкт
	 * @Param event Інформація про подію
	 */
	public void handleEvent(Observable observable, Object event);
}
